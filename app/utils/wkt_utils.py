def convert_list_point_to_wkt(point: list[float]) -> str:
    return f"POINT({point[0]} {point[1]})"


def convert_polygon_to_list(polygon: str) -> list[list[float]]:
    return [
        [float(point.split()[0]), float(point.split()[1])]
        for point in str(polygon)[10:-2].split(", ")
    ]


def convert_list_polygon_to_wkt(polygon: list[list[float]]) -> str:
    return f'POLYGON(({", ".join([f"{point[0]} {point[1]}" for point in polygon])}))'
