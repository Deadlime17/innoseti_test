from app.schemas.order import OrderCreateModel, OrderModel
from app.schemas.couriers import CourierModel
from app.schemas.areas import AreaModel
from fastapi import APIRouter, HTTPException
from app.db import order as order_db

router = APIRouter()


@router.post("/order", response_model=OrderModel, status_code=201)
async def create_order(order: OrderCreateModel):
    area = await order_db.find_area_by_point(order.point)
    if not area:
        raise HTTPException(status_code=404, detail="Area not found")

    courier = await order_db.find_courier_by_area_id(area["id"])
    if not courier:
        raise HTTPException(status_code=404, detail="Courier not found")

    order = order.dict()
    order["area_id"] = area["id"]
    order["courier_id"] = courier["id"]
    result = await order_db.create_order(order)
    courier_model = CourierModel(**courier, area_id=area["id"])
    area_model = AreaModel(**area)
    return OrderModel(**result, courier=courier_model, area=area_model)
