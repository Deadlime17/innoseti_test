from app.schemas.areas import *
from app.db import area as area_db
from fastapi import APIRouter
from fastapi import HTTPException
from asyncpg.exceptions import UniqueViolationError


router = APIRouter()


@router.post("/area", response_model=AreaModel, status_code=201)
async def create_area(area: AreaCreateModel):
    if intersected_polygon := await area_db.intersection_polygon(area.polygon):
        raise HTTPException(
            status_code=409,
            detail=f"Area intersect with another area with id {intersected_polygon['id']}",
        )
    try:
        result = await area_db.create_area(area.dict())
    except UniqueViolationError:
        raise HTTPException(
            status_code=409, detail=f"Area with name '{area.name}' already exists"
        )

    return {"id": result["id"], "name": area.name}


@router.get("/area/{area_id}", response_model=FullAreaModel)
async def get_area(area_id: int):
    result = await area_db.get_area(area_id)
    if result:
        return result
    raise HTTPException(status_code=404, detail=f"Area with id {area_id} not found")


@router.get("/areas", response_model=AreasModel)
async def get_areas():
    areas = await area_db.get_areas()
    return {"areas": areas}
