from app.schemas.couriers import CourierCreateModel, CourierModel, CouriersModel
from app.db import courier as courier_db
from fastapi import APIRouter, HTTPException
from asyncpg.exceptions import UniqueViolationError


router = APIRouter()


@router.post("/courier", response_model=CourierModel, status_code=201)
async def create_courier(courier: CourierCreateModel):
    try:
        result = await courier_db.create_courier(courier.dict())
    except UniqueViolationError:
        raise HTTPException(
            status_code=404, detail=f"Courier with name '{courier.name}' already exists"
        )
    return {"id": result["id"], "name": courier.name, "area_id": courier.area_id}


@router.get("/courier/{courier_id}", response_model=CourierModel)
async def get_courier(courier_id: int):
    result = await courier_db.get_courier(courier_id)
    if result:
        return result
    raise HTTPException(
        status_code=404, detail=f"Courier with id {courier_id} not found"
    )


@router.get("/couriers", response_model=CouriersModel)
async def get_couriers():
    couriers = await courier_db.get_couriers()
    return {"couriers": couriers}
