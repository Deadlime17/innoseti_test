from pydantic import BaseModel


class CourierCreateModel(BaseModel):
    """Validate request data"""

    name: str
    area_id: int


class CourierModel(BaseModel):
    id: int
    name: str
    area_id: int


class CouriersModel(BaseModel):
    couriers: list[CourierModel]

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "couriers": [
                    {"id": 1, "name": "Иванов Иван Иванович", "area_id": 1},
                    {"id": 2, "name": "Петров Петр Петрович", "area_id": 2},
                ]
            }
        }
