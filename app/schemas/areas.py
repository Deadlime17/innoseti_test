from pydantic import BaseModel, validator


class AreaCreateModel(BaseModel):
    """Validate request data"""

    name: str
    polygon: list[list[float]]

    @validator("polygon")
    def validate_geom(cls, polygon):
        if len(polygon) < 3:
            raise ValueError("Polygon must have at least 3 points")

        if not all([len(point) == 2 for point in polygon]):
            raise ValueError("Each point must have 2 coordinates")
        return polygon

    @validator("polygon")
    def close_polygon(cls, polygon):
        if polygon[0] != polygon[-1]:
            polygon.append(polygon[0])
        return polygon

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "name": "Ленинский район",
                "polygon": [
                    [0.0, 255.0],
                    [255.0, 255.0],
                    [255.0, 0.0],
                    [0.0, 0.0],
                ],
            }
        }


class AreaModel(BaseModel):
    id: int
    name: str


class FullAreaModel(AreaModel):
    polygon: list[list[float]]

    class Config:
        orm_mode = True


class AreasModel(BaseModel):
    areas: list[AreaModel]

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "areas": [
                    {"id": 1, "name": "Ленинский район"},
                    {"id": 2, "name": "Советский район"},
                ]
            }
        }
