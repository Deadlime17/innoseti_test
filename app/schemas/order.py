from pydantic import BaseModel, validator
from datetime import datetime
from .areas import AreaModel
from .couriers import CourierModel


class OrderCreateModel(BaseModel):
    """Validate request data"""

    point: list[float]

    @validator("point")
    def validate_point(cls, point):
        if len(point) != 2:
            raise ValueError("Point must have at 2 coordinates")
        return point

    class Config:
        orm_mode = True
        schema_extra = {"example": {"point": [50.0, 100.0]}}


class OrderModel(BaseModel):
    id: int
    datetime: datetime
    area: AreaModel
    courier: CourierModel
