from app.models.database import database
from app.models.areas import area_table
from geoalchemy2.shape import to_shape
from sqlalchemy import select, func
from app.utils.wkt_utils import convert_list_polygon_to_wkt, convert_polygon_to_list


async def intersection_polygon(polygon: list[list[float]]) -> dict:
    query = area_table.select().where(
        func.ST_Intersects(area_table.c.polygon, convert_list_polygon_to_wkt(polygon))
    )
    result = await database.fetch_one(query)
    if result:
        return dict(result)


async def create_area(area: dict) -> dict:
    area["polygon"] = convert_list_polygon_to_wkt(area["polygon"])
    query = area_table.insert().values(area).returning()
    result = await database.fetch_one(query)
    return dict(result)


async def get_area(area_id: int) -> dict:
    query = area_table.select().where(area_table.c.id == area_id)
    result = await database.fetch_one(query)
    if result:
        result = dict(result)
        result["polygon"] = convert_polygon_to_list(to_shape(result["polygon"]))
        return result


async def get_areas() -> list[dict]:
    query = select(area_table.c.id, area_table.c.name).order_by(area_table.c.id)
    result = await database.fetch_all(query)
    return list(map(dict, result))


#
# async def update_area(area_id: int, area: AreaCreateModel):
#     query = (
#         area_table.update()
#         .where(area_table.c.id == area_id)
#         .values(area.dict())
#         .returning(area_table.c.id)
#     )
#     return await database.fetch_one(query)
#
#
# async def delete_area(area_id: int):
#     query = area_table.delete().where(area_table.c.id == area_id)
#     return await database.execute(query)
#
