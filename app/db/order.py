from app.models.database import database
from app.models.orders import orders_table
from app.models.couriers import courier_table
from app.models.areas import area_table
from sqlalchemy import select, func
from app.utils.wkt_utils import convert_list_point_to_wkt


async def find_courier_by_point(point: list[float]):
    j = area_table.join(courier_table, area_table.c.id == courier_table.c.area_id)
    query = (
        select(
            [
                courier_table.c.id.label("courier_id"),
                courier_table.c.name.label("courier_name"),
                area_table.c.id.label("area_id"),
                area_table.c.name.label("area_name"),
            ]
        )
        .select_from(j)
        .where(func.ST_Contains(area_table.c.polygon, convert_list_point_to_wkt(point)))
    )

    result = await database.fetch_one(query)
    if result:
        return dict(result)


async def find_area_by_point(point: list[float]):
    query = select([area_table.c.id, area_table.c.name]).where(
        func.ST_Contains(area_table.c.polygon, convert_list_point_to_wkt(point))
    )
    result = await database.fetch_one(query)
    if result:
        return dict(result)


async def find_courier_by_area_id(area_id: int):
    query = select([courier_table.c.id, courier_table.c.name]).where(
        courier_table.c.area_id == area_id
    )
    result = await database.fetch_one(query)
    if result:
        return dict(result)


async def create_order(order: dict):
    order["point"] = convert_list_point_to_wkt(order["point"])
    query = (
        orders_table.insert()
        .values(order)
        .returning(orders_table.c.id, orders_table.c.datetime)
    )
    result = await database.fetch_one(query)
    return dict(result)
