from app.models.database import database
from app.models.couriers import courier_table


async def create_courier(courier: dict) -> dict:
    query = courier_table.insert().values(courier).returning()
    result = await database.fetch_one(query)
    return dict(result)


async def get_courier(courier_id: int):
    query = courier_table.select().where(courier_table.c.id == courier_id)
    return await database.fetch_one(query)


async def get_couriers() -> list[dict]:
    query = courier_table.select().order_by(courier_table.c.id)
    result = await database.fetch_all(query)
    return list(map(dict, result))


#
#
# async def update_courier(courier_id: int, courier: CourierCreateModel):
#     query = (
#         courier_table.update()
#         .where(courier_table.c.id == courier_id)
#         .values(courier.dict())
#         .returning(courier_table.c.id)
#     )
#     return await database.fetch_one(query)
#
#
# async def delete_courier(courier_id: int):
#     query = courier_table.delete().where(courier_table.c.id == courier_id)
#     return await database.execute(query)
#
