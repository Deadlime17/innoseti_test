from fastapi import FastAPI
from app.models.database import database
from app.routers import areas, couriers, orders
import uvicorn


app = FastAPI(title="Delivery API", version="0.1.0")


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(areas.router)
app.include_router(couriers.router)
app.include_router(orders.router)

if __name__ == "__main__":
    uvicorn.run("app.main:app", host="0.0.0.0", port=5000)
