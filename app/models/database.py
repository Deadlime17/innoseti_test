from os import environ
import databases


DB_USER = environ.get("DB_USER", "delivery")
DB_PASSWORD = environ.get("DB_PASSWORD", "delivery")
DB_HOST = environ.get("DB_HOST", "localhost")
DB_NAME = "delivery"
DB_PORT = environ.get("DB_PORT", "5434")

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)

database = databases.Database(SQLALCHEMY_DATABASE_URL)
