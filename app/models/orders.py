from sqlalchemy import Column, Integer, Table, MetaData, ForeignKey, DateTime, func
from geoalchemy2 import Geometry
from .areas import area_table
from .couriers import courier_table


metadata = MetaData()


orders_table = Table(
    "orders",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("datetime", DateTime(timezone=True), server_default=func.now()),
    Column("point", Geometry(geometry_type="POINT", spatial_index=True)),
    Column("area_id", ForeignKey(area_table.c.id)),
    Column("courier_id", ForeignKey(courier_table.c.id)),
)
