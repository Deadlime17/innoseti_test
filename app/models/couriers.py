from sqlalchemy import Column, Integer, String, Table, MetaData, ForeignKey
from .areas import area_table


metadata = MetaData()

courier_table = Table(
    "couriers",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(100), unique=True),
    Column("area_id", ForeignKey(area_table.c.id)),
)
