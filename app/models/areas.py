from sqlalchemy import Column, Integer, String, Table, MetaData
from geoalchemy2 import Geometry


metadata = MetaData()

area_table = Table(
    "areas",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(100), unique=True),
    Column("polygon", Geometry(geometry_type="POLYGON", spatial_index=True)),
)
