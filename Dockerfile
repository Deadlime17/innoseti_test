ARG PYTHON_VERSION=3.9.15
FROM python:${PYTHON_VERSION}

ARG POETRY_VERSION=1.1.13
RUN useradd -d /home/delivery_api -m delivery_api
WORKDIR /home/delivery_api

RUN pip install poetry==${POETRY_VERSION}

COPY pyproject.toml poetry.lock ./

RUN poetry config virtualenvs.create false
RUN poetry install --no-dev --no-interaction --no-ansi

COPY alembic.ini ./
COPY app ./app
COPY migrations ./migrations

USER delivery_api

